import { Request } from "express";
import { UnauthorizedException } from "../exceptions";
import { User, userRepo, UserStatus } from "../models";
import { AUTH_PREFIX, TokenType, verifyToken } from "../utils";

export async function authMiddleware(req: Request, res, next) {
    try {
        const auth = req.headers.authorization;
        if(!auth) return next();
        const authParams = auth.split(' ');
        if(authParams.length != 2 || authParams[0] != AUTH_PREFIX)
            return next(new UnauthorizedException('auth prefix wrong'));
        
        const payload: any = verifyToken(authParams[1], TokenType.ACCESS);
        const user: User = await userRepo().findOne(payload.id);
        if(!user || user.status == UserStatus.REMOVED)
            return next(new UnauthorizedException('user not found'));
        req.user = user;
        next();
    } catch(err) {
        next(err);
    }
}