import { body, ValidationChain, validationResult } from "express-validator";
import { InvalidParametersException } from "../exceptions";

export function validatorMiddleware(chain: ValidationChain[]) {
    return async function validate(req, res, next) {
        await Promise.all(chain.map(rule => rule.run(req)));
        const result = validationResult(req);
        if(result.isEmpty()) return next();
        next(new InvalidParametersException(result.array()));
    }
}