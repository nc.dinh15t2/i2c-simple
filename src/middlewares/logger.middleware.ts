import { logger } from "../utils";
import { Request, Response } from 'express';

export function loggerHandler(req: Request, res: Response, next) {
    const oldJson = res.json;
    res.json = function (body): any {
        logger.info(`[${req.originalUrl}]: ${JSON.stringify(body)}`);
        return oldJson.call(res, body);
    }
    next();
}