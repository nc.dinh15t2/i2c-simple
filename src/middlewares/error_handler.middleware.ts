import { BaseException, InvalidParametersException } from "../exceptions";
import { logger } from "../utils";

export function errorHandler(err: Error, req, res, next) {
    if(err instanceof BaseException) {
        res.status(err.getStatus())
            .json(err.getResponse());
    } else {
        res.status(500)
            .json({
                message: 'Something wrong',
                code: '10001'
            });
    }
    logger.error(err);
}