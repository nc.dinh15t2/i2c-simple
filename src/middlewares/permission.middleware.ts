import { ForbiddenException } from "../exceptions";
import { BasePermission } from "../permissions";

export function permissionMiddleware(permissions: BasePermission[]) {
    return function(req, res, next) {
        for(let permission of permissions) {
            if(!permission.isAccepted(req)) 
                return next(new ForbiddenException(permission.code, permission.internalMessage));
        }
        next();
    }
}