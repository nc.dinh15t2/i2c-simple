import multer from "multer";
import path from "path";
import sharp from "sharp";
import { v4 } from "uuid";

export const uploadMiddleware = multer({
    limits: {
        fileSize: 4 * 1024 * 1024
    }
})