import { Router } from "express";
import { AuthControllerV1 } from "../controllers";

const authRouterV1 = Router();

authRouterV1.post('/login', AuthControllerV1.login);
authRouterV1.post('/exchange-token', AuthControllerV1.exchangeToken);

export default authRouterV1;