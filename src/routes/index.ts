import { Router } from 'express';
import { authMiddleware } from '../middlewares';
import { Endpoint } from '../utils';
import authRouterV1 from './auth.route';
import commentRouterV1 from './comment.route';
import imageRouterV1 from './image.route';
import userRouterV1 from './user.route';
import uploadCertificateRouterV1 from './upload_certificate.route';

const apiRouter = Router();

//v1
const routerV1 = Router();

routerV1.use(Endpoint.AUTH, authRouterV1);
routerV1.use(Endpoint.USERS, userRouterV1);
routerV1.use(Endpoint.IMAGES, imageRouterV1);
routerV1.use(Endpoint.COMMENTS, commentRouterV1);
routerV1.use(Endpoint.UPLOAD_CERTIFICATES, uploadCertificateRouterV1);

apiRouter.use(Endpoint.V1, authMiddleware, routerV1);



export default apiRouter;