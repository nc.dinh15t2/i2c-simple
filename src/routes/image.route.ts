import { Router } from "express";
import { ImageControllerV1 } from "../controllers";

const imageRouterV1 = Router();
imageRouterV1.get('/', ImageControllerV1.list);
imageRouterV1.post('/', ImageControllerV1.upload);
imageRouterV1.get('/:id', ImageControllerV1.detail);

export default imageRouterV1;