import { Router } from "express";
import { UserControllerV1 } from "../controllers";

const userRouterV1 = Router();
userRouterV1.get('/', UserControllerV1.list);
userRouterV1.post('/', UserControllerV1.create);
userRouterV1.get('/:id', UserControllerV1.detail);
userRouterV1.put('/:id/avatars', UserControllerV1.updateAvatar);

export default userRouterV1;