import { Router } from "express";
import { UploadCertificateControllerV1 } from "../controllers";

const uploadCertificateRouterV1 = Router();
uploadCertificateRouterV1.post('/', UploadCertificateControllerV1.create);

export default uploadCertificateRouterV1;
