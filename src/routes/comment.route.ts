import { Router } from "express";
import { CommentControllerV1 } from "../controllers";

const commentRouterV1 = Router();

commentRouterV1.get('/', CommentControllerV1.list);
commentRouterV1.post('/', CommentControllerV1.create);

export default commentRouterV1;