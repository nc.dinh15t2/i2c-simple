import { Types } from "mongoose";
import { v4 } from "uuid";
import { Image, ImageModel, ImageStatus, serverRepo } from "../models";
import { ImageSizeLarge, ImageSizeMedium, ImageSizeSmall, saveImage, SERVER_ID } from "../utils";

export module ImageServiceV1 {
    export async function uploadImage(uploaderId: number, buffer: Buffer) {
        const url = v4();
        let image = new Image();
        image.uploaderId = uploaderId;
        image.url = url;
        image.status = ImageStatus.UPLOADING;
        image.serverId = SERVER_ID;
        image.small = {
            localName: 'small.png',
            size: {
                width: ImageSizeSmall.WIDTH,
                height: ImageSizeSmall.HEIGHT
            }
        };
        image.medium = {
            localName: 'medium.png',
            size: {
                width: ImageSizeMedium.WIDTH,
                height: ImageSizeMedium.HEIGHT
            }
        };
        image.large = {
            localName: 'large.png',
            size: {
                width: ImageSizeLarge.WIDTH,
                height: ImageSizeLarge.HEIGHT
            }
        };
        image = await ImageModel.create(image);
        await saveImage(buffer, url);
        image.status = ImageStatus.UPLOADED;
        await ImageModel.updateOne({_id: image.id}, image);
        return image;
    }

    export async function getByIds(ids: string[]) {
        const images: Image[] = await ImageModel.find({_id: {$in: ids.map(id => new Types.ObjectId(id))}});
        const result = {};
        const server = await serverRepo().findOne(SERVER_ID);
        images.forEach(image => { 
            result[image._id.toHexString()] = image;
            image['domain'] = server.domain;
        });
        return result;
    }
}