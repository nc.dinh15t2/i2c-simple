import { BadRequestException } from "../exceptions";
import { User, userRepo, UserStatus } from "../models";
import { generateToken, TokenExp, TokenType, verifyPassword, verifyToken } from "../utils";
export module AuthServiceV1 {
    export async function login({identity, password}) {
        const user: User = await userRepo().createQueryBuilder()
                        .where('email = :identity OR username = :identity', {identity: identity})
                        .andWhere('status IN (:status)', {status: [UserStatus.ACTIVE, UserStatus.INACTIVE]})
                        .getOne();

        if(!user || !await verifyPassword(password, user.encrypted))
            throw new BadRequestException('Wrong username or password', '0008');
        
        const accessToken = generateToken({
            id: user.id
        }, TokenType.ACCESS, TokenExp.ACCESS);

        const refreshToken = generateToken({
            id: user.id
        }, TokenType.REFRESH, TokenExp.REFRESH);
        
        return {accessToken, user, refreshToken};
    }

    export async function exchangeToken({refreshToken}) {
        const payload: any = verifyToken(refreshToken, TokenType.REFRESH);
        const user: User = await userRepo().createQueryBuilder()
                            .where('id = :id', {id: payload.id})
                            .getOne();
        if(!user)
            throw new BadRequestException('Refresh token invalid', '0009');
        
        const accessToken = generateToken({
            id: user.id
        }, TokenType.ACCESS, TokenExp.ACCESS);

        return { accessToken };
    }
} 