import { FileType, Server, uploadCertificateRepo, User } from "../models";
import { Endpoint, generateToken, getEndpointByFileType, TokenExp, TokenType } from "../utils";

export module UploadCertificateServiceV1 {
    export async function create(user: User, server: Server, fileType: FileType) {
        const uploadCertificate = uploadCertificateRepo().create({
            server: server,
            user: user,
            type: fileType
        });
        await uploadCertificateRepo().save(uploadCertificate);
        const uploadToken = generateToken({
            id: uploadCertificate.id,
            serverId: server.id,
            userId: user.id
        }, TokenType.UPLOAD_FILE, TokenExp.UPLOAD_FILE);
        return {
            endpoint: [server.domain, Endpoint.API, Endpoint.V1, getEndpointByFileType(fileType)].join(''),
            token: uploadToken,
        };
    }

    export async function getById(id: number) {
        return await uploadCertificateRepo().findOne(id);
    }
}