import { BadRequestException } from "../exceptions";
import { userRepo, User, UserStatus } from "../models";
import { encryptPassword, getPagination, verifyPassword } from "../utils";

export module UserServiceV1 {
    export async function list(params, p, limit) {
        const offset = p*limit;
        return await userRepo().createQueryBuilder()
            .select()
            .offset(offset)
            .limit(limit)
            .getManyAndCount();
    }

    export async function create({
        username, 
        email, 
        name: {
            first, 
            last
        }, 
        gender, 
        password}) {
        const encrypted: string = await encryptPassword(password);
        let user: User = userRepo().create({
            username,
            email,
            name: {
                first,
                last
            },
            gender,
            encrypted,
            status: UserStatus.INACTIVE
        });
        user = await userRepo().save(user);
        return user;
    }

    export async function updateAvatar(userId: number, imageId: string) {
        const resp = await userRepo().update(userId, {
            avatarId: imageId
        });
        return resp;
    }

    export async function changePassword(user: User, {oldPassword, password}) {
        if(!await verifyPassword(oldPassword, user.encrypted))
            throw new BadRequestException('oldPassword is wrong', '0012');
        user.encrypted = await encryptPassword(password);
        await userRepo().save(user);
        return true;
    }
}