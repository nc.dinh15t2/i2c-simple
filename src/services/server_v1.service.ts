import { serverRepo } from "../models";

export module ServerServiceV1 {
    export async function getByIds(ids: string[]) {
        const servers = await serverRepo().findByIds(ids);
        return servers.reduce((result, server) => {
            result[server.id] = server; 
            return result;
        }, {});
    }

    export async function getAll() {
        return await serverRepo().find();
    }
}