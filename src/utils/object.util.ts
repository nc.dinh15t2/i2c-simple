import { InternalServerException, WrongSkeletonTypeException } from "../exceptions";

interface BaseExtractorOptions {
    aliasOf?: string;
}

abstract class BaseExtractor {
    public aliasOf?: string;
    abstract extract(initObject);
    constructor({aliasOf}: BaseExtractorOptions) {
        this.aliasOf = aliasOf;
    }
    withAliasOf(aliasOf: string): BaseExtractor {
        this.aliasOf = aliasOf;
        return this;
    }
}

export enum NormalType {
    ALL = 'any',
    INT = 'int',
    FLOAT = 'float',
    STRING = 'string',
    BOOL = 'bool',
    CUSTOM = 'custom'
}

export interface NormalExtractorOptions extends BaseExtractorOptions {
    type?: string|NormalType;
    parseFunc?: Function;
}
export class NormalExtractor extends BaseExtractor {
    private type: NormalType;
    private parseFunc?: Function;
    constructor(props: NormalExtractorOptions) {
        super(props);
        let type = props.type || NormalType.STRING;
        let parseFunc = props.parseFunc;
        if(type === NormalType.CUSTOM && !parseFunc)
            throw new InternalServerException('0008', 'parseFunc must be a function in type custom');

        this.type = type as NormalType;
        this.parseFunc = parseFunc;
    }
    extract(initObject) {
        if(initObject == null) return null;
        switch(this.type) {
            case NormalType.STRING:
                return initObject.toString();
            case NormalType.INT:
                return parseInt(initObject);
            case NormalType.FLOAT:
                return parseFloat(initObject);
            case NormalType.CUSTOM:
                return this.parseFunc.call(null, initObject);
            default:
                return initObject;
        }
    }
}

export interface ArrayExtractorOptions extends BaseExtractorOptions {
    childType: string|NormalType|BaseExtractor;
}
export class ArrayExtractor extends BaseExtractor {
    private childType: BaseExtractor;
    constructor(props: ArrayExtractorOptions) {
        super(props);
        let childType = props.childType;
        if(!(childType instanceof BaseExtractor)) {
            childType = new NormalExtractor({type: childType as NormalType});
        }
        this.childType = childType;
    }
    extract(initObject: any[]) {
        if(initObject == null) return null;
        const result = [];
        const childType = this.childType;
        initObject.forEach(value => {
            result.push(childType.extract(value));
        });
        return result;
    }
}

export interface ObjectExtractorOptions extends BaseExtractorOptions {
    fields: object;
}
export class ObjectExtractor extends BaseExtractor {
    private fields: object;
    constructor(props: ObjectExtractorOptions) {
        super(props);
        let fields = props.fields;
        try {
            Object.keys(fields).forEach(key => {
                if(fields[key] instanceof BaseExtractor) return;
                fields[key] = new NormalExtractor({type: fields[key] as NormalType});
            });
        } catch(err) {
            throw new InternalServerException('0009', 'field values must me NormalType or instance of BaseExtractor');
        }
        this.fields = fields;
    }
    extract(initObject) {
        if(initObject == null) return null;
        const result = {};
        const fields = this.fields;
        Object.keys(fields).forEach(key => {
            const field = fields[key] as BaseExtractor;
            if(!field) return;
            if(field.aliasOf) {
                let cur = initObject;
                for(let name of field.aliasOf.split('.')) {
                    cur = cur[name];
                }
                result[key] = field.extract(cur);
            } else {
                result[key] = field.extract(initObject[key]);
            }
            
        });
        
        return result;
    }
}