export module DefaultResponse {
    export const SUCCESS = {
        message: 'Successful'
    };
}

export function getPagination(data, p: number, limit: number, total: number) {
    return {
        status: 200,
        message: 'Successful',
        data,
        meta: {
            p,
            limit,
            total,
            numPages: Math.ceil(total/limit)
        }
    };
}

export function getResponseSuccess(data) {
    return {
        status: 200,
        message: 'Successful',
        data,
    };
}

export function getCreateSuccess(data) {
    return {
        status: 200,
        message: 'Create Successfully',
        data,
    };
}

export function getDeleteSuccess(data) {
    return {
        status: 200,
        message: 'Delete Successfully',
        data,
    };
}