export module Endpoint {
    export const API = '/api';
    export const V1 = '/1.0';
    export const RESOURCES = '/resources';
    export const AUTH = '/auth';
    export const USERS = '/users';
    export const IMAGES = '/images';
    export const COMMENTS = '/comments';
    export const UPLOAD_CERTIFICATES = '/upload_certificates';
}