export function getRandomFloat(): number {
    return Math.random();
}

export function getRandomInt(min: number, max: number): number {
    return min + Math.floor(getRandomFloat()*(max-min));
}