import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { BadRequestException, UnauthorizedException } from '../exceptions';
import { FileType } from '../models';
import { SECRET_KEY } from './config.util';
import { Endpoint } from './constant.util';
import { logger } from './logger.util';

export enum TokenType {
    ACCESS = 'access',
    REFRESH = 'refresh',
    UPLOAD_FILE = 'upload_file',
}

export function encryptPassword(password): Promise<string> {
    return bcrypt.hash(password, 12);
}

export function verifyPassword(password, encrypted): Promise<boolean> {
    return bcrypt.compare(password, encrypted);
}

const TOKEN_TYPE = '___token_type';

export function generateToken(payload, type: TokenType, expiresIn): string {
    payload[TOKEN_TYPE] = type;
    return jwt.sign(payload, SECRET_KEY, {
        expiresIn: expiresIn
    });
}

export function verifyToken(token, type: TokenType, isThrow = true) {
    try {
        let payload = jwt.verify(token, SECRET_KEY);
        if(payload[TOKEN_TYPE] !== type) 
            throw new UnauthorizedException('wrong token type');
        delete payload[TOKEN_TYPE];
        return payload;
    } catch (err) {
        logger.error(err);
        if(isThrow) {
            throw new UnauthorizedException('Token invalid');
        } else {
            return false;
        }
    }
}

export function getEndpointByFileType(fileType: FileType) {
    switch(fileType) {
        case FileType.IMAGE:
            return Endpoint.IMAGES;
    }
}