import dotenv from 'dotenv';
dotenv.config();

export const MongoConfig = {
    URL: process.env.MONGO_URL,
    SCHEMA: process.env.MONGO_SCHEMA
}

export const MongoConfig2 = {
    URL: process.env.MONGO_URL_2,
    SCHEMA: process.env.MONGO_SCHEMA_2
}

export const MySqlConfig = {
    HOST: process.env.DB_HOST,
    PORT: parseInt(process.env.DB_PORT),
    USER: process.env.DB_USER,
    PASS: process.env.DB_PASS,
    SCHEMA: process.env.DB_SCHEMA
}

export const TokenExp = {
    ACCESS: process.env.ACCESS_TOKEN_EXP,
    REFRESH: process.env.REFRESH_TOKEN_EXP,
    UPLOAD_FILE: process.env.UPLOAD_FILE_TOKEN_EXP,
}

export const PORT = parseInt(process.env.PORT);
export const SECRET_KEY = process.env.SECRET_KEY;
export const DEBUG_MODE = process.env.DEBUG_MODE === 'true';
export const LOG_DIR = process.env.LOG_DIR;
export const AUTH_PREFIX = process.env.AUTH_PREFIX;
export const FILE_STORAGE = process.env.FILE_STORAGE;
export const SERVER_ID = process.env.SERVER_ID;

export module ImageSizeSmall {
    export const WIDTH = 50;
    export const HEIGHT = 50;
}

export module ImageSizeMedium {
    export const WIDTH = 150;
    export const HEIGHT = 150;
}

export module ImageSizeLarge {
    export const WIDTH = 500;
    export const HEIGHT = 500;
}