import { EventEmitter2 } from "eventemitter2";
import { EventEmitter } from "events";
import * as fs from "fs";
import path from "path";
import sharp from "sharp";
import { BadRequestException } from "../exceptions";
import { ImageModel, ImageStatus } from "../models";
import { FILE_STORAGE, ImageSizeLarge, ImageSizeMedium, ImageSizeSmall } from "./config.util";
import { logger } from "./logger.util";

export interface ImageSize {
    width: number;
    height: number;
}

function makeDir(dirPath: string) {
    return new Promise((resolve, reject) => {
        fs.mkdir(dirPath, { recursive: true }, (err, path) => {
            if(err) return reject(err);
            resolve(path);
        });
    });
}

abstract class AbstractImageUtil {
    public fileSize: ImageSize;
    protected buffer: Buffer;
    protected type: string;
    public path: string;
    protected parentPath: string;
    localName: string;

    constructor(parentPath: string, buffer: Buffer) {
        this.parentPath = parentPath;
        this.buffer = buffer;
    }

    public async saveFile() {
        await makeDir(path.join(FILE_STORAGE, this.parentPath));
        this.localName = `${this.type}.png`;
        this.path = path.join(this.parentPath, this.localName);
        const fullPath = path.join(FILE_STORAGE, this.path);
        await sharp(this.buffer)
            .resize(this.fileSize.width, this.fileSize.height, {
                fit: sharp.fit.inside,
                withoutEnlargement: true
            })
            .toFile(fullPath);
    }
}

export class SmallImageUtil extends AbstractImageUtil {
    fileSize = { width: ImageSizeSmall.WIDTH, height: ImageSizeSmall.HEIGHT};
    protected type = 'small';
}

export class MediumImageUtil extends AbstractImageUtil {
    fileSize = { width: ImageSizeMedium.WIDTH, height: ImageSizeMedium.HEIGHT};
    protected type = 'medium';
}

export class LargeImageUtil extends AbstractImageUtil {
    fileSize = { width: ImageSizeLarge.WIDTH, height:ImageSizeLarge.HEIGHT};
    protected type = 'large';
}

export function readFile(filePath: string): Promise<Buffer> {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, (err, data: Buffer) => {
            if(err) return reject(err);
            resolve(data); 
        });
    });
}

export async function saveImage(buffer: Buffer, imagePath: string) {
    const large = new LargeImageUtil(imagePath, buffer);
    await large.saveFile();

    const largeBuffer: Buffer = await readFile(path.join(FILE_STORAGE, large.path));
    const medium = new MediumImageUtil(imagePath, largeBuffer);
    await medium.saveFile();

    const mediumBuffer: Buffer = await readFile(path.join(FILE_STORAGE, medium.path));
    const small = new SmallImageUtil(imagePath, mediumBuffer);
    await small.saveFile();
}

export async function removeFile(filePath: string) {
    return new Promise((resolve, reject) => {
        fs.rmdir(path.join(FILE_STORAGE, filePath), { recursive: true }, (err) => {
            if(err) return reject(err);
            resolve(true);
        })
    });
}

export const eventEmitter = new EventEmitter2({
    ignoreErrors: false
});

export enum EventName {
    REMOVE_IMAGE = 'remove_image',
};

eventEmitter.on(EventName.REMOVE_IMAGE, async (imageId) => {
    try {
        const image = await ImageModel.findById(imageId);
        if(image && image.status == ImageStatus.UPLOADED) {
            removeFile(image.url);
            image.status = ImageStatus.REMOVED;
            await ImageModel.updateOne({_id: image.id}, image);
        }
    } catch(err) {
        logger.error(err);
    }
});