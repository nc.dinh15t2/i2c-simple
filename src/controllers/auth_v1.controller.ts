import { AuthDtoV1 } from "../dtos";
import { AuthRuleV1 } from "../rules";
import { AuthServiceV1 } from "../services";
import { initControllerV1 } from "./base.controller";

export module AuthControllerV1 {
    export const login = initControllerV1({
        validators: AuthRuleV1.login,
        handler: async function (req, res, next) {
            let tokenInfo = await AuthServiceV1.login(req.body);
            res.json(AuthDtoV1.accessToken.extract(tokenInfo));
        }
    })

    export const exchangeToken = initControllerV1({
        validators: AuthRuleV1.exchangeToken,
        handler: async function (req, res, next) {
            let token = await AuthServiceV1.exchangeToken(req.body);
            res.json(token);
        }
    })
}