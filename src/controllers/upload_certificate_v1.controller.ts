import { UploadCertificatePermissionV1 } from "../permissions";
import { UploadCertificateRuleV1 } from "../rules";
import { ServerServiceV1 } from "../services";
import { UploadCertificateServiceV1 } from "../services/upload_certificate_v1.service";
import { getCreateSuccess, getRandomInt } from "../utils";
import { initControllerV1 } from "./base.controller";

export module UploadCertificateControllerV1 {
    export const create = initControllerV1({
        permissions: UploadCertificatePermissionV1.create,
        validators: UploadCertificateRuleV1.create,
        handler: async function (req, res) {
            const servers = await ServerServiceV1.getAll();
            const choosedServer = servers[getRandomInt(0, servers.length)];
            const uploadCertificate = 
                await UploadCertificateServiceV1.create(req.user, choosedServer, req.body.type);
            res.json(getCreateSuccess(uploadCertificate));
        }
    })
}