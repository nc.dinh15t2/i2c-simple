import { ImageDtoV1 } from "../dtos";
import { BadRequestException } from "../exceptions";
import { uploadMiddleware } from "../middlewares";
import { Image, ImageModel, serverRepo, UploadCertificatePayload, uploadCertificateRepo, UploadCertificateStatus } from "../models";
import { ImagePermissionV1 } from "../permissions";
import { ImageRuleV1 } from "../rules";
import { ImageServiceV1, UploadCertificateServiceV1 } from "../services";
import { getCreateSuccess, getResponseSuccess, SERVER_ID, TokenType, verifyToken } from "../utils";
import { initControllerV1 } from "./base.controller";

export module ImageControllerV1 {
    export const list = initControllerV1({
        handler: async function (req, res) {
            res.json(await ImageModel.find());
        }
    });

    export const detail = initControllerV1({
        handler: async function(req, res) {
            const image = await ImageModel.findById(req.params.id);
            const server = await serverRepo().findOne(image.serverId);
            image['domain'] = server.domain;
            const imageDto = ImageDtoV1.image.extract(image);
            res.json(getResponseSuccess(imageDto));
        }
    })

    export const upload = initControllerV1({
        permissions: ImagePermissionV1.upload,
        middlewares: [uploadMiddleware.single('image')],
        handler: async function (req, res) {
            const payload = verifyToken(req.headers['upload_token'], TokenType.UPLOAD_FILE, false) as UploadCertificatePayload;
            if(!payload) 
                throw new BadRequestException('Upload token invalid', '0049');
            if (payload.userId != req.user.id || payload.serverId != SERVER_ID)
                throw new BadRequestException('Token is not apply to this user or server', '0050');
            const uploadCertificate = await UploadCertificateServiceV1.getById(payload.id);
            if (uploadCertificate.status != UploadCertificateStatus.WAITING)
                throw new BadRequestException('Upload certificate is used', '0051');
            uploadCertificate.status = UploadCertificateStatus.USED;
            uploadCertificateRepo().save(uploadCertificate);
            const image = await ImageServiceV1.uploadImage(req.user.id, req.file.buffer);
            res.json(getCreateSuccess({id: image.id}));
        }
    })
} 