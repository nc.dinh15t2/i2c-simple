import { UserDtoV1 } from "../dtos";
import { BadRequestException, NotfoundException } from "../exceptions";
import { Image, ImageModel, ImageStatus, userRepo } from "../models";
import { UserPermissionV1 } from "../permissions";
import { UserRuleV1 } from "../rules";
import { ImageServiceV1, UserServiceV1 } from "../services";
import { DefaultResponse, eventEmitter, EventName, getPagination, getResponseSuccess } from "../utils";
import { initControllerV1 } from "./base.controller";

export module UserControllerV1 {
    export const list = initControllerV1({
        permissions: UserPermissionV1.list,
        validators: UserRuleV1.list,
        handler: async function (req, res, next) {
            const p = req.query.p || 0;
            const limit = req.query.limit || 10;
            const [users, total] = await UserServiceV1.list({}, p, limit);
            const avatarIds = users.reduce((result, user) => { 
                if(user.avatarId) result.push(user.avatarId);
                return result;
            }, []);

            const avatars = await ImageServiceV1.getByIds(avatarIds);
            const userWithAvatars = users.map(user => {
                user['avatar'] = avatars[user.avatarId];
                return user;
            });

            const userDtos = UserDtoV1.listShort.extract(userWithAvatars);
            res.json(getPagination(userDtos, p as number, limit as number, total));
        }
    });

    export const detail = initControllerV1({
        permissions: UserPermissionV1.detail,
        handler: async function (req, res, next) {
            const userId = req.params.id == 'me' ? req.user.id : req.params.id;
            const user = await userRepo().findOne(userId);
            const avatars = await ImageServiceV1.getByIds([user.avatarId]);
            user['avatar'] = Object.values(avatars)[0];
            const userDto = UserDtoV1.full.extract(user);
            res.json(userDto);
        }
    });

    export const create = initControllerV1({
        validators: UserRuleV1.create,
        handler: async function (req, res, next) {
            const user = await UserServiceV1.create(req.body);
            res.json(UserDtoV1.full.extract(user));
        }
    });

    export const updateAvatar = initControllerV1({
        permissions: UserPermissionV1.uploadAvatar,
        validators: UserRuleV1.updateAvatar,
        handler: async function (req, res) {
            const userId = req.params.id == 'me' ? req.user.id : req.params.id;
            const user = await userRepo().findOne(userId);
            if (!user)
                throw new NotfoundException();
                
            const image: Image = await ImageModel.findById(req.body.imageId);

            if(!image || image.uploaderId != userId) 
                throw new BadRequestException('Image not found', '0052');
            if(image.status !== ImageStatus.UPLOADED)
                throw new BadRequestException('Image not available', '0053');

            eventEmitter.emitAsync(EventName.REMOVE_IMAGE, user.avatarId);
            user.avatarId = image.id;
            await userRepo().save(user);
            res.json(getResponseSuccess({id: user.id}));
        }
    });

    export const changePassword = initControllerV1({
        permissions: UserPermissionV1.changePassword,
        validators: UserRuleV1.changePassword,
        handler: async function (req, res) {
            await UserServiceV1.changePassword(req.user, req.body);
            res.json(DefaultResponse.SUCCESS);
        }
    });
};