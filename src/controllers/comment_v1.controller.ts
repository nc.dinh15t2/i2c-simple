import { Comment, CommentModel, ImageModel } from "../models";

export module CommentControllerV1 {
    export async function list(req, res, next) {
        try {
            res.json(await CommentModel.find());
        } catch(err) {
            next(err);
        }
    }
    export async function create(req, res, next) {
        try {
            const comment = {
                content: 'abcxyz'
            } as Comment;
            await CommentModel.create(comment);
            res.json(comment);
        } catch(err) {
            next(err);
        }
    }
}