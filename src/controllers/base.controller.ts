import { RequestHandler } from "express";
import { ValidationChain } from "express-validator";
import { permissionMiddleware, validatorMiddleware } from "../middlewares";
import { BasePermission } from "../permissions";

export interface InitControllerV1 {
    handler: RequestHandler;
    permissions?: BasePermission[];
    validators?: ValidationChain[];
    middlewares?: RequestHandler[];
}

export function initControllerV1(params: InitControllerV1) {
    return [
        permissionMiddleware(params.permissions || []),
        validatorMiddleware(params.validators || []),
        ...(params.middlewares || []),
        async function(req, res, next) {
            try {
                await params.handler.call(null, req, res, next);
            } catch(err) {
                next(err);
            }
        }
    ];
}