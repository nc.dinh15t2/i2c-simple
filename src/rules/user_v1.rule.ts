import { body, CustomValidator, param, query } from "express-validator";
import { Gender, User, userRepo } from "../models";

export module UserRuleV1 {
    export const notExistedEmail: CustomValidator = async email => {
        const count = await userRepo()
            .createQueryBuilder()
            .where('email = :email', {email})
            .getCount();
        if(count !== 0) return Promise.reject('Email is existed');
    }

    export const notExistedUsername: CustomValidator = async username => {
        const count = await userRepo()
            .createQueryBuilder()
            .where('username = :username', {username})
            .getCount();
        if(count !== 0) return Promise.reject('Username is existed');
    }

    export const list = [
        query('p').optional().isInt({min: 0}).default(0).toInt(),
        query('limit').optional().isInt({min: 1, max: 100}).default(10).toInt()
    ];

    export const create = [
        body('username')
            .notEmpty()
            .isString()
            .isLength({min: 5, max: 10})
            .custom(notExistedUsername),
        body('email')
            .notEmpty()
            .isEmail()
            .custom(notExistedEmail),
        body('password')
            .notEmpty()
            .isString()
            .isLength({min:5, max: 10}),
        body('name')
            .isObject({strict: true}),
        body('name.first')
            .isString()
            .isLength({max: 100}),
        body('name.last')
            .notEmpty()
            .isString()
            .isLength({max: 50}),
        body('gender')
            .isIn(Object.values(Gender))
    ];

    export const changePassword = [
        body('oldPassword')
            .notEmpty()
            .isString(),
        body('password')
            .notEmpty()
            .isString()
    ];

    export const updateAvatar = [
        body('imageId')
            .notEmpty()
            .isString()
            .isLength({max: 255})
    ];
}