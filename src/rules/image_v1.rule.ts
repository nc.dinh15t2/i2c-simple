import { body } from "express-validator";

export module ImageRuleV1 {
    export const upload = [
        body('file').notEmpty(),
        body('token').notEmpty().isString().isLength({max: 255})
    ];
}