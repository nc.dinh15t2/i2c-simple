import { body } from "express-validator";

export module AuthRuleV1 {
    export const login = [
        body('identity')
            .notEmpty()
            .isString()
            .isLength({max: 255}),
        body('password')
            .notEmpty()
            .isString()
            .isLength({max: 255})
    ];

    export const exchangeToken = [
        body('refreshToken')
            .notEmpty()
            .isString()
    ]
}