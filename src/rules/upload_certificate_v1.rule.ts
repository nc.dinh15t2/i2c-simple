import { body } from "express-validator";
import { FileType } from "../models";

export module UploadCertificateRuleV1 {
    export const create = [
        body('type').isIn(Object.values(FileType))
    ]
}