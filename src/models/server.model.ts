import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity({name: 'servers'})
export class Server {
    @PrimaryColumn({
        length: 10
    })
    id: string;

    @Column()
    domain: string;
}