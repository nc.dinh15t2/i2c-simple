import { createConnection, Connection } from 'mongoose';
import { MongoConfig, MongoConfig2 } from '../utils';

export const mongoConnection = createConnection(MongoConfig.URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    dbName: MongoConfig.SCHEMA
});