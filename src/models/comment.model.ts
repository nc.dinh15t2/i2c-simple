import { getModelForClass, prop } from "@typegoose/typegoose";
import { mongoConnection } from "./mongo.connection";

export class Comment {
    @prop()
    public content: string;
}

export const CommentModel = getModelForClass(Comment, {
    existingConnection: mongoConnection
});