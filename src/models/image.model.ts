import { getModelForClass, ModelOptions, prop } from '@typegoose/typegoose';
import { TimeStamps } from '@typegoose/typegoose/lib/defaultClasses';
import { Types } from 'mongoose';
import { mongoConnection } from './mongo.connection';

export enum ImageStatus {
    UPLOADING = 'uploading',
    UPLOADED = 'uploaded',
    REMOVED = 'removed'
}

@ModelOptions({
    schemaOptions: {
        _id: false
    }
})
export class ImageSize {
    @prop()
    width: number;

    @prop()
    height: number;
}

@ModelOptions({
    schemaOptions: {
        _id: false
    }
})
export class ImageDetail {
    @prop({
        alias: 'local_name'
    })
    localName: string;

    @prop()
    size: ImageSize;
}

@ModelOptions({
    existingConnection: mongoConnection,
    schemaOptions: {
        collection: 'images',
        versionKey: false,
        timestamps: true
    }
})
export class Image extends TimeStamps {
    @prop({auto: true})
    _id: Types.ObjectId;

    get id() {
        return this._id.toHexString();
    }

    @prop()
    serverId: string;

    @prop()
    uploaderId: number;

    @prop()
    url: string;

    @prop()
    small: ImageDetail;

    @prop()
    medium: ImageDetail;

    @prop()
    large: ImageDetail;

    @prop({ enum: ImageStatus, default: ImageStatus.UPLOADING })
    status: ImageStatus;
}

export const ImageModel = getModelForClass(Image);