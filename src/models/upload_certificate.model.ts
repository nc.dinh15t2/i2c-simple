import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { Server } from "./server.model";
import { User } from "./user.model";

export enum UploadCertificateStatus {
    WAITING = 'W',
    USED = 'U'
}

export enum FileType {
    IMAGE = 'image'
}

@Entity({
    name: 'upload_certificates'
})
export class UploadCertificate {
    @PrimaryGeneratedColumn({
        type: 'bigint',
        unsigned: true
    })
    id: number;

    @ManyToOne(type => Server, {
        lazy: true,
    })
    @JoinColumn()
    server: Server;

    @ManyToOne(type => User, {
        lazy: true
    })
    user: User;

    @Column({
        type: 'char',
        length: 1,
        default: UploadCertificateStatus.WAITING
    })
    status: UploadCertificateStatus;

    @Column({
        length: 10,
    })
    type: FileType;

    @CreateDateColumn({
        name: 'created_at',
        type: 'timestamp',
    })
    createdAt: Date;
}

export interface UploadCertificatePayload {
    id: number;
    serverId: string;
    userId: number;
}