import { Connection, createConnection, Repository } from "typeorm";
import { MySqlConfig } from "../utils";
import { Server } from "./server.model";
import { UploadCertificate } from "./upload_certificate.model";
import { User } from "./user.model";

let connection: Connection;
let _userRepo;
let _serverRepo;
let _uploadCertificateRepo;

export function userRepo(): Repository<User> {
    return _userRepo;
}

export function serverRepo(): Repository<Server> {
    return _serverRepo;
}

export function uploadCertificateRepo(): Repository<UploadCertificate> {
    return _uploadCertificateRepo;
}

(async () => {
    connection = await createConnection({
        type: 'mysql',
        host: MySqlConfig.HOST,
        port: MySqlConfig.PORT,
        username: MySqlConfig.USER,
        password: MySqlConfig.PASS,
        database: MySqlConfig.SCHEMA,
        synchronize: true,
        logger: 'advanced-console',
        logging: true,
        entities: [
            User,
            Server,
            UploadCertificate,
        ]
    });

    _userRepo = await connection.getRepository(User);
    _serverRepo = await connection.getRepository(Server);
    _uploadCertificateRepo = await connection.getRepository(UploadCertificate);
    console.log('Connected to mysql');
})();
