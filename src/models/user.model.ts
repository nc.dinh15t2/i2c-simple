import { BeforeInsert, BeforeUpdate, Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

export class UserName {
    first: string;
    last: string;
}

export enum UserStatus {
    ACTIVE = 'A',
    BLOCKED = 'B',
    DISABLED = 'D',
    REMOVED = 'R',
    INACTIVE = 'I'
}

export enum Gender {
    MALE = 'M',
    FEMALE = 'F',
    OTHER = 'O'
}

@Entity({
    name: 'users'
})
export class User {
    @PrimaryGeneratedColumn({
        type: 'mediumint',
        unsigned: true
    })
    id: number;

    @Column({
        name: 'username',
        unique: true,
        length: 50
    })
    username: string;

    @Column({
        name: 'email',
        unique: true
    })
    email: string;

    @Column({
        name: 'encrypted',
        type: 'text'
    })
    encrypted: string;

    @Column({
        name: 'name',
        type: 'json'
    })
    name: UserName;

    @Column({
        type: 'char',
        length: 1,
        default: UserStatus.ACTIVE
    })
    status: UserStatus;

    @Column({
        type: 'char',
        length: 1,
        default: Gender.OTHER
    })
    gender: Gender;

    @Column({
        name: 'avatar_id',
        nullable: true
    })
    avatarId: string;

    @UpdateDateColumn({
        type: 'timestamp',
        name: 'updated_at',
    })
    updatedAt: Date;

    @CreateDateColumn({
        name: 'created_at',
        type: 'timestamp',
    })
    createdAt: Date;
}