import { ObjectExtractor } from "../utils";

export module ImageDtoV1 {
    export const size = new ObjectExtractor({
        fields: {
            width: 'int',
            height: 'int'
        }
    });

    export const imageDetail = new ObjectExtractor({
        fields: {
            localName: 'string',
            size: size
        }
    });

    export const image = new ObjectExtractor({
        fields: {
            domain: 'string',
            url: 'string',
            small: imageDetail,
            medium: imageDetail,
            large: imageDetail
        }
    });
}