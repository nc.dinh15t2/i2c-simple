import { User } from '../models';
import { ArrayExtractor, NormalExtractor, NormalType, ObjectExtractor } from '../utils';
import { ImageDtoV1 } from './image_v1.dto';

export module UserDtoV1 {
    export const short = new ObjectExtractor({
        fields: {
            id: 'int',
            username: 'string',
            email: 'string',
            name: new ObjectExtractor({
                fields: {
                    first: 'string',
                    last: 'string'
                }
            }),
            avatar: ImageDtoV1.image
        }
    });

    export const listShort = new ArrayExtractor({
        childType: short
    });

    export const full = new ObjectExtractor({
        fields: {
            id: 'int',
            username: 'string',
            email: 'string',
            name: new ObjectExtractor({
                fields: {
                    first: 'string',
                    last: 'string'
                }
            }),
            avatar: ImageDtoV1.image,
            gender: 'string',
            status: 'string',
            created: 'int'
        }
    });
   
    export const listFull = new ArrayExtractor({
        childType: full
    });
}