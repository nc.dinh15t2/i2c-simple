import { NormalExtractor, ObjectExtractor } from "../utils";
import { UserDtoV1 } from "./user_v1.dto";

export module AuthDtoV1 {
    export const accessToken = new ObjectExtractor({
        fields: {
            accessToken: 'string',
            refreshToken: 'string',
            userInfo: UserDtoV1.full.withAliasOf('user')
        }
    })
}