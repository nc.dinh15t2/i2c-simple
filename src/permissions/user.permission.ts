import { UserStatus } from "../models";
import { AuthWithStatuses } from "./base.permission";

export module UserPermissionV1 {
    export const list = [
        new AuthWithStatuses(UserStatus.ACTIVE)
    ];

    export const detail = [
        new AuthWithStatuses(UserStatus.ACTIVE)
    ]

    export const uploadAvatar = [
        new AuthWithStatuses(UserStatus.ACTIVE)
    ]

    export const changePassword = [
        new AuthWithStatuses(UserStatus.ACTIVE)
    ]
}