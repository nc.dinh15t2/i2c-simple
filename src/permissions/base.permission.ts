import { Request } from "express";
import { UserStatus } from "../models";

export abstract class BasePermission {
    public code: string = '0003';
    public internalMessage: string = 'No message';
    abstract isAccepted(req: Request): boolean;
}

export class AuthWithStatuses extends BasePermission {
    private statuses: UserStatus[];
    constructor(...statuses) {
        super();
        this.statuses = statuses;
    }

    isAccepted(req) {
        return req.user && this.statuses.includes(req.user.status);
    }
}