import { UserStatus } from "../models";
import { AuthWithStatuses } from "./base.permission";

export module ImagePermissionV1 {
    export const upload = [
        new AuthWithStatuses(UserStatus.ACTIVE)
    ];
}