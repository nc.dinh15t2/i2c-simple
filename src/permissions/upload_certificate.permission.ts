import { UploadCertificate, UserStatus } from "../models";
import { AuthWithStatuses } from "./base.permission";

export module UploadCertificatePermissionV1 {
    export const create = [
        new AuthWithStatuses(UserStatus.ACTIVE)
    ];
}