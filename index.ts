const express = require('express');
import cors from 'cors';
import { Application } from 'express';
import { errorHandler, loggerHandler } from './src/middlewares';
import apiRouter from './src/routes';
import { Endpoint, FILE_STORAGE, PORT } from './src/utils';

const app: Application = express();

app.get('/check-app-version', (req, res) => {
    res.json({
        version: '1.0.0'
    });
});

app.use(cors({
    origin: 'http://localhost:3000',
    optionsSuccessStatus: 200
}));
app.use(loggerHandler);
app.use(express.json());
app.use(Endpoint.API, apiRouter);
app.use(errorHandler);
app.use(Endpoint.RESOURCES, express.static(FILE_STORAGE));

const server = app.listen(PORT, () => {
    console.log(`Listening on port: ${PORT}`);
});
